import Vue from 'vue';
import VueRouter from 'vue-router';
// import Home from '../views/Home.vue';
const home = () => import('../views/Home.vue');
const d1 = () => import(/* webpackChunkName: "d1" */'../views/D1.vue');
const VuexStore = () => import('../views/VuexStore.vue');

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    // component: Home,
    component: home,
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    path: '/d1',
    name: 'd1',
    component: d1,
  },
  {
    path: '/VuexStore',
    name: 'VuexStore',
    component: VuexStore,
  },
  // {
  //   path: '*',
  //   redirect: '/',
  // },
  {
    path: '*',
    redirect: { name: 'Home' },
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
