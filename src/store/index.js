import Vue from 'vue';
import Vuex from 'vuex';
import api from '@/service/api';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    a: 0,
    b: 0,
    api: null,
  },
  getters: {
    c: (state) => state.a + state.b,
    cEx: (state) => (data) => state.a + state.b + data,
  },
  mutations: {
    set(state, data) {
      state[data.name] = data.value;
    },
  },
  actions: {
    callAPI({ commit, state }, payload) {
      console.log('in store before call api', state.api);
      return api.api1().then((res) => {
        console.log('res', res);
        commit('set', { name: 'api', value: payload });
        console.log('in store after call api', state.api);
        return res;
      });
    },
  },
  modules: {
  },
});
